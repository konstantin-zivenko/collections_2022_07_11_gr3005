# namedtuple
# obj.name

from collections import namedtuple

def custom_divmod(x, y):
    DivMod = namedtuple("DivMod", "quotient remainder") # ["quotient", "remainder"]
    return  DivMod(*divmod(x, y))

["Adam Smith", 35, "data engeneer", 2000]

Employee = namedtuple("Employee", "name age position salary")

a = Employee("Adam Smith", 35, "data engeneer", 2000)

# deque
# LIFO, FIFO - стеки
# O(n)

from collections import deque

ticket_queue = deque()

ticket_queue.append("Olga")
ticket_queue.append("Sergiy")
ticket_queue.append("Kateryna")
ticket_queue.append("Andriy")

left1 = ticket_queue.popleft()

ticket_queue_2 = deque('qwerty', 7)


#OrderedDict






